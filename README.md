# LLVM Fedora/EL ABI

This repository holds ABI data that help to validate the ABI compatibility of
LLVM libraries across different Fedora releases.

## Instructions to update this repository

In order to update the ABI dumps in this repository, run:

``` sh
./refresh.sh
```

Then commit the compressed files using [git-ls](https://git-lfs.com).


As new Fedora releases are announced, the variable `supported_mock_roots` need
to be updated. It's recommended to leave at least `N-2` distro releases
listed there. Older releases may not be needed because they do not receive
frequent updates.

### Different architectures

While it's technically possible to generate all ABI dumps from a single
computer via emulation, this support has been disabled in the script because
each ABI dump takes many hours to get generated via emulation.
