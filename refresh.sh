#!/usr/bin/env bash

# Generate up-to-date .abi files for all supported Fedora releases and
# architectures.

# List of distros and architectures that we want to generate ABI dumps.
# In other words: this is the list of mock roots that we'd like to be
# ABI-compatible.
# Missing mock roots:
# - i386: debuginfo packages are not available.
# - fedora-39-s390x: debuginfo packages are not available yet.
supported_mock_roots="
fedora-38-aarch64
fedora-38-ppc64le
fedora-38-s390x
fedora-38-x86_64
fedora-39-aarch64
fedora-39-ppc64le
fedora-39-x86_64"


arch=$(rpm -E "%{_arch}")

is_emulated () {
    case $1 in
        *-$arch) return 1;;
        *-i386)
            if [[ "x$arch" == "xx86_64" ]]; then
                return 1
            fi;;
    esac

    return 0
}

dump_abi () {
    libname=$1
    pkg=$2
    libregex=$3
    headers=$4

    abi_file=$libname.$($mock_cmd --chroot "rpm -q $pkg").abi
    if [[ -e $abi_file.xz ]]; then
        return 0;
    fi

    version=$($mock_cmd --chroot "rpm -q --qf '%{V}-%{R}.%{ARCH}\n' $pkg")
    library=$($mock_cmd --chroot "rpm -ql $pkg" \
            | grep -E $libregex | sort | head -n 1)
    debuginfo=$($mock_cmd --chroot "rpm -ql $pkg-debuginfo" \
            | grep -E $libregex'.*\.debug')

    if [[ "x$pkg" == "xllvm-libs" ]]; then
        $mock_cmd --chroot "nm $debuginfo \
            | awk '/T _LLVM/ || /T LLVM/ { print $3 }' | sort -u \
            | sed -e 's/^_//g' | cut -d ' ' -f 3 > /builddir/llvm.symbols"
        mock_extra_args="-symbols-list /builddir/llvm.symbols"
    fi

    $mock_cmd --chroot "abi-dumper -quiet $mock_extra_args \
        -lver $version \
        -skip-cxx \
        -public-headers $headers \
        --search-debuginfo=/usr/lib/debug \
        -o /builddir/$abi_file $library"

    $mock_cmd --copyout "/builddir/$abi_file" ./

    sed -i 's/LLVM_[0-9]\+/LLVM_NOVERSION/' $abi_file

    xz $abi_file
}

set -e

for mock_root in $supported_mock_roots; do
    # Avoid emulation because it takes too long (many hours per ABI dump).
    if is_emulated $mock_root; then
        continue
    fi

    mock_cmd="mock -r $mock_root --isolation=simple"

    $mock_cmd --init
    $mock_cmd --install abi-compliance-checker llvm-devel llvm-libs \
        clang-devel clang-libs 'dnf-command(debuginfo-install)'
    $mock_cmd --dnf-cmd debuginfo-install llvm-libs clang-libs

    dump_abi 'libLLVM' 'llvm-libs' 'libLLVM-.*.so' '/usr/include/llvm-c'
    dump_abi 'libclang' 'clang-libs' 'libclang\.so\..*' '/usr/include/clang-c'
done
